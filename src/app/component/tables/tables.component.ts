import { Component, OnInit } from '@angular/core';
import { TablesService } from './tables.service';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})

export class TablesComponent implements OnInit {

  sortList: any;
  groupMonth: any = [];
  constructor(private svcTb: TablesService) { }

  ngOnInit() {
      // init read sales.json
      this.getSales();
  }

  getSales() {
    this.svcTb.readTables()
      .subscribe(sales =>{
        this.sortTotal(sales.json())
      });
  }

  sortTotal(sales){ // order by total
    this.sortList = sales.resourceList.sort((a, b) => parseFloat(a.total) - parseFloat(b.total));
    // send table
    this.groupByMonth(this.sortList);
  }

  groupByMonth(sales){
    // order by date
    sales.sort(function(a, b) {
      a = new Date(a.date);
      b = new Date(b.date);
      return a < b ? -1 : a > b ? 1 : 0;
  });
    // format col date
    for (let i = 0; i < sales.length; i++){
      sales[i].date = sales[i].date.split("-")[0] + "-" + sales[i].date.split("-")[1];
    }
    // group by sum
    for (let i = 0, j = 0; i < sales.length; i++,j++){
      let item = sales[i];
      if(i < sales.length - 1){
        if( sales[i].date == sales[i+1].date){
          item.total = sales[i].total + sales[i+1].total;
          i++;
        }
        this.groupMonth[j] = item;
      }
      if(i == sales.length - 1){
        this.groupMonth[j] = item;
      }
    }
  }
}
