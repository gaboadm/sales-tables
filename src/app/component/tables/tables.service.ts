import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {Observable} from 'rxjs';
@Injectable()
export class TablesService {

  constructor(private http: Http) { }

  readTables() : Observable <any> {
      return this.http.get('assets/sales.json');
  }
}
